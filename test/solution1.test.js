import assert from 'assert';
import solutions from '../src/solutions.js';

describe('Problem 1 testing', () => {
  it('should return [ 4 ]', () => {
    assert.deepStrictEqual(solutions.filterSubstract([ 3, 1, 4, 2]), [4])
  });
  it('should return [ 10 ]', () => {
    assert.deepStrictEqual(solutions.filterSubstract([ -3, 10, 4, 5]), [10])
  });
  it('should return [ 4, 4 ]', () => {
    assert.deepStrictEqual(solutions.filterSubstract([ 3, 1, 4, 2, 3, 4]), [4, 4])
  });
}); 