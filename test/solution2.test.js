import assert from 'assert';
import solutions from '../src/solutions.js';

describe('Problem 2 testing', () => {
  it('should return [ 1, 2, 3 ]', () => {
    assert.deepStrictEqual(solutions.filterDividedBy([ 1, 2, 3, 4], 4), [1, 2, 3])
  });
  it('should return [ ]', () => {
    assert.deepStrictEqual(solutions.filterDividedBy([ 1, 2, 3, 4], 1), [])
  });
  it('should return [ 1, 2 ]', () => {
    assert.deepStrictEqual(solutions.filterDividedBy([ 1, 2, 3, 3], 3), [1, 2])
  });
  it('should return [ 1, 2 ]', () => {
    assert.deepStrictEqual(solutions.filterDividedBy([ 1, 2, 3, 3], 4), [1, 2, 3, 3])
  });
}); 