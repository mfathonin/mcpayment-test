import solutions from './solutions.js';
import { testCase } from './test-case.js';

console.log('Problem #1:');
testCase['problem1'].forEach(test =>
  console.log(test.nums, '=>', solutions.filterSubstract(test.nums))
)

console.log('Problem #2:');
testCase['problem2'].forEach(test =>
  console.log(test.nums, '=>', solutions.filterDividedBy(test.nums, test.x))  
)

console.log('Problem #3:');
testCase['problem3'].forEach(test =>
  console.log(solutions.filterWord(test.word, test.x))
)