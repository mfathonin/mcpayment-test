export const testCase = {
    'problem1': [
        { nums: [3, 1, 4, 2]},
        { nums: []},
        { nums: [4]},
        { nums: [-1, -2, -3, -4]},
        { nums: [1, -2, 3, -4, 5, -6]},
        { nums: [-1, 2, -3, 4, -5, 6]},
        { nums: [ 1, 2, 4, 1, 2, 4]}
    ],
    'problem2': [
        { nums: [1, 2, 3, 4], x:4 },
        { nums: [1, 2, 3, 4], x:1 },
        { nums: [], x:4 },
        { nums: [4], x:4 },
        { nums: [-1, -2, -3, -4], x:4 },
        { nums: [1, -2, 3, -4, 5, -6], x:4 },
        { nums: [-1, 2, -3, 4, -5, 6], x:4 }
    ],
    'problem3': [
        { word: 'souvenir loud four lost', x: 4 },
        { word: 'souvenir loud four lost', x: 8 },
        { word: 'hello world good morning', x: 3 },
        { word: 'hello world good morning', x: 4 },
        { word: 'hello world good morning', x: 5 }
    ]
}