const filterSubstract = (nums) => {
  const temp = nums;
  const ret = [];
  temp.forEach(testNumber => {
    const isX = nums.filter(subtractor => (testNumber - subtractor) < 0);

    if (isX.length === 0) ret.push(testNumber);
  });

  return ret;
}

const filterDividedBy = (nums, x) => {
  const temp = nums;
  const ret = [];
  temp.forEach(testNumber => {
    const isX = nums.filter(divider => (testNumber / divider) === x);

    if (isX.length === 0) ret.push(testNumber);
  });

  return ret;
}

const filterWord = (word, x) => {
  const ret = word.split(' ').filter(word => word.length == x);
  return ret;
}

export default {
  filterSubstract,
  filterDividedBy,
  filterWord
}